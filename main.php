<?php

/* pour le chargement automatique des classes d'Eloquent (dans le répertoire vendor) */
require_once 'vendor/autoload.php';
require_once 'src/mf/utils/AbstractClassLoader.php';
require_once 'src/mf/utils/ClassLoader.php';

    $loader = new \mf\utils\ClassLoader('src');
    $loader->register();


use \tweeterapp\model\Tweet as Tweet;
use \tweeterapp\model\Like as Like;
use \tweeterapp\model\User as User;
use \tweeterapp\model\Follow as Follow;

$config = parse_ini_file('conf/config.ini');

/* une instance de connexion  */
$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection( $config ); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* rendre la connexion visible dans tout le projet */
$db->bootEloquent();           /* établir la connexion */

\tweeterapp\view\TweeterView::addStyleSheet('html/style.css');

$router = new \mf\router\Router();

$router->addRoute('home', '/home/', '\tweeterapp\control\TweeterController', 'viewHome', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->setDefaultRoute('/home/');

$router->addRoute('viewtweet', '/view/', '\tweeterapp\control\TweeterController', 'viewTweet', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('viewusertweets', '/user/', '\tweeterapp\control\TweeterController', 'viewUserTweets', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('posttweet', '/post/', '\tweeterapp\control\TweeterController', 'viewPostTweet', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('sendtweet', '/send/', '\tweeterapp\control\TweeterController', 'viewSendTweet', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('login', '/login/', '\tweeterapp\control\TweeterAdminController', 'login', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('logout', '/logout/', '\tweeterapp\control\TweeterAdminController', 'logout', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('signup', '/signup/', '\tweeterapp\control\TweeterAdminController', 'signup', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('checklogin', '/check_login/', '\tweeterapp\control\TweeterAdminController', 'checkLogin', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('checksignup', '/check_signup/', '\tweeterapp\control\TweeterAdminController', 'checkSignup', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_NONE);

$router->addRoute('following', '/following/', '\tweeterapp\control\TweeterController', 'following', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('follow', '/follow/', '\tweeterapp\control\TweeterController', 'follow', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('follower', '/follower/', '\tweeterapp\control\TweeterController', 'follower', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_USER);

$router->addRoute('followee', '/followee/', '\tweeterapp\control\TweeterController', 'followee', \tweeterapp\auth\TweeterAuthentification::ACCESS_LEVEL_USER);

$router->run();
