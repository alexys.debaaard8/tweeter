<?php

namespace mf\utils;

    class ClassLoader extends AbstractClassLoader{
        public function getFilename(string $classname) : string{
            $path=str_replace("\\", DIRECTORY_SEPARATOR, $classname);
            $path.='.php';
            return $path;
        }

        public function makePath(string $filename) : string{
            return $this->prefix.DIRECTORY_SEPARATOR.$filename;
        }

        public function loadClass(string $classname){
            $chemin=$this->getFilename($classname);
            $chemin=$this->makePath($chemin);

            if(file_exists($chemin)){
                require_once $chemin;
            }

        }

    }
?>