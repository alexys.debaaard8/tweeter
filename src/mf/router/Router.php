<?php

namespace mf\router;
use \mf\auth\Authentification as Authentification;

class Router extends AbstractRouter {

    public function __construct(){
        parent::__construct();
    }

    public function addRoute($name, $url, $ctrl, $mth, $accessLevel){
         self::$routes[$url] = [$ctrl, $mth, $accessLevel];
         self::$aliases[$name]= $url;
    }

    public function setDefaultRoute($url){
         self::$aliases['default'] = $url;
    }
    
    public function urlFor($route_name, $param_list=[]){
        $debut=$this->http_req->script_name;
        $url=$debut;
        $aliasUrl=self::$aliases[$route_name];
        $url.=$aliasUrl;
        if(!empty($param_list)){
            $par=array_shift($param_list);
            $url.='?'.$par[0].'='.$par[1];
            while(!empty($param_list)){
                $par=array_shift($param_list);
                $url.='&'.$par[0].'='.$par[1];
            }
        }
        return $url;
    }

    public function run(){
        session_start();
        $url= self::$aliases['default'];
        if($this->http_req->path_info!==null){ 
            $url=$this->http_req->path_info;
            $check= new Authentification();
            $verif=$check->checkAccessRight(self::$routes[$url][2]);
            if($verif){
                $ctrl=self::$routes[$url][0];
                $mth=self::$routes[$url][1];
                $c=new $ctrl();
                $c->$mth();
            }
            else{
                $this->executeRoute('default');
            }
        }
    }

    public static function executeRoute($alias){
        $url=self::$aliases[$alias];
        $ctrl=self::$routes[$url][0];
        $mth=self::$routes[$url][1];
        $c=new $ctrl();
        $c->$mth();
    }
}