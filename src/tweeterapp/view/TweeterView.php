<?php

namespace tweeterapp\view;

class TweeterView extends \mf\view\AbstractView {
  
    public function __construct( $data ){
        parent::__construct($data);
    }

    private function renderHeader(){
        $router=new \mf\router\Router();
        $html = '<h1>MiniTweeTR</h1>';
        $html.= $this->renderTopMenu();

        return $html;
    }

    protected function renderTopMenu(){
        $router = new \mf\router\Router();
        $auth = new \mf\auth\Authentification();  
        $urlHome = $router->urlFor("default");
        $html='<nav id="navbar"><a class="tweet-control" href="'.$urlHome.'">
        <img alt="home" src="../../html/home.png"></a>';

        if(isset($_SESSION['user_login'])){    
            $urlfollow = $router->urlFor("following");
            $urldeco = $router->urlFor("logout");
            $html .='<a class="tweet-control" href="'.$urlfollow.'">
            <img src="../../html/followees.png"></a>
            <a class="tweet-control" href="'.$urldeco.'">
            <img src="/../../html/logout.png"></a>';
        }
        else{
            $urlLogin = $router->urlFor("login");
            $urlSignup = $router->urlFor("signup");
            $html .= '<a class="tweet-control" href="'.$urlLogin.'">
            <img alt="login" src="../../html/login.png"></a>
            <a class="tweet-control" href="'.$urlSignup.'">
            <img alt="signup" src="../../html/signup.png"></a></nav>';
        }
        return $html;
    }
    
    protected function renderBottomMenu(){
        return $body='<nav id="menu" class="theme-backcolor1"> 
        <div id="nav-menu"><div class="button theme-backcolor2"><a href="../../main.php/post/">New</a></div></div></nav>';

        return $html;
    }

    private function renderFooter(){
        return 'La super app créée en Licence Pro &copy;2021';
    }
    
    
    private function renderHome(){
        $html="";
        foreach($this->data as $tweet){
            $html.= $this->renderATweet($tweet);
        }
        return $html;
          
    }

    private function renderATweet($tweet){
        $html="";
        $router=new \mf\router\Router();
        $author = $tweet->author()->first();
        $url_tweet=$router->urlFor('viewtweet', [['id', $tweet->id]]);
        $url_user=$router->urlFor('viewusertweets', [['id', $author->id]]);
            
        $html.="<div class='tweet'>";
        $html.="<span class='tweet-author'><a href=".$url_user.">$author->username </a> </span>";
        $html.="<a href=".$url_tweet."><div class='tweet-text'>$tweet->text </div></a>  ";  
        $html.="<div class='tweet-footer'><span class='tweet-timestamp'>$tweet->created_at </span></div>";   
        if(isset($_SESSION['user_login'])){
            $url_follow = $router->urlFor("follow", [['id', $author->id]]);
            $html.="<hr><a class='tweet-control' href=".$url_follow."><img alt='Follow' src='../../html/follow.png'></a></div></div>";
        }
        else{
            $html.="</div>";
        }

        return $html;
          
    }
     
    private function renderUserTweets(){
        $user=$this->data;
    
        $tweets=$user->tweets()->orderBy('created_at', 'DESC')->get();

        $html= "<h2>Tweets from ".$user->username."</h2>";
        $html.="<h3>$user->followers follower(s)</h3>";

        foreach($tweets as $tweet){
            $html.= $this->renderATweet($tweet);   
        }
        return $html;

    }
  
    private function renderViewTweet(){
        $tweet=$this->data;
        $html = $this->renderATweet($tweet);
        return $html;
        
    }

    /* Méthode renderPostTweet
     *
     * Realise la vue de régider un Tweet
     *
     */
    protected function renderPostTweet(){
        $router = new \mf\router\Router();
        $urlSend = $router->urlFor("sendtweet");
        $html=<<<EOT
        <form action="$urlSend" method=post>
            <textarea id="tweet-form" name=text placeholder="Enter your tweet...", maxlength=140></textarea>
            <div><input id="send_button" type=submit name=send value="Send"></div>
        </form>
        EOT;

        return $html;
        
    }

    protected function renderSignUp(){
        $router = new \mf\router\Router();
        $url = $router->urlFor("checksignup");
        $html=<<<EOT
        <form class="forms" action="$url" method=post>
            <input class="forms-text" type=text name=fullname placeholder="full Name">
            <input class="forms-text" type=text name=username placeholder="username">
            <input class="forms-text" type=password name=password placeholder="password">
            <input class="forms-text" type=password name=password_verify placeholder="retype password">
            <button class="forms-button" name=login_button type="submit">Create</button>
        </form>
        EOT;

        return $html;
    }

    protected function renderLogin(){
        $router = new \mf\router\Router();
        $url = $router->urlFor("checklogin");
        $html=<<<EOT
        <form class="forms" action="$url" method=post>
        <input class="forms-text" type=text name=username placeholder="username">
        <input class="forms-text" type=password name=password placeholder="password">
        <button class="forms-button" name=login_button type="submit">Login</button>
        </form> 
        EOT;
        return $html;
    }



    protected function renderFollowing(){
        $router = new \mf\router\Router();
        $user=$this->data;
        $following = $user->follows()->get(); 
        $urlFollower = $router->urlFor("follower");
        $nbfollowers = $user->followedBy()->count();

        $html='<h3>'.$user->username.' vous avez : <a href="'.$urlFollower.'"> '.$nbfollowers.' followers </a> </h3>';

        $html.="<h2>Tweets from following</h2>";
        foreach ($following as $user) {
            $tweets = $user->tweets()->get();
            foreach($tweets as $OneTweet){
                $html.=$this->renderATweet($OneTweet);
            }
        }
        return $html;
    }

    protected function renderFollower(){
        $router = new \mf\router\Router();
        $user = $this->data;
        $followers = $user->followedBy()->get(); 
        
        $html ='<h3>Vos followers :</h3>';

        foreach ($followers as $follower) {
            $url_user=$router->urlFor('viewusertweets', [['id', $follower->id]]);
            $html.= '<p><a href="'.$url_user.'">'.$follower->username.'</a></p>';
        }   

        return $html;
    }
    

    protected function renderFollowee(){
        $router = new \mf\router\Router();
        $user=$this->data;
        $followers = $user->follows()->get(); 
        $html="<h2>Mes followings : <h2>";
        foreach($followers as $follower ){
            $url_user=$router->urlFor('viewusertweets', [['id', $follower->id]]);
            $html.= '<p><a href="'.$url_user.'">'.$follower->username.'</a></p>';
        }
                 
        return $html;
    }

    protected function renderBody($selector){

        $body="<header class='theme-backcolor1'>".$this->renderHeader()."</header>";
        $body.='<section>
                <article class="theme-backcolor2">';
        if($selector=="home"){
            $body.="<h2>Latest Tweets</h2>";
            $body.=$this->renderHome();
        }
        if($selector=="viewtweet"){
            $body.=$this->renderViewTweet();
        }
        if($selector=="usertweets"){
            $body.=$this->renderUserTweets();
        }
        if($selector=="posttweet"){
            $body.=$this->renderPostTweet();
        }
        if($selector=="login"){
            $body.=$this->renderLogin();
        }
        if($selector=="signup"){
            $body.=$this->renderSignUp();
        }
        if($selector=="following"){
            $body.=$this->renderFollowing();
        }
        if($selector=="follower"){
            $body.=$this->renderFollower();
        }
        if($selector=="followee"){
            $body.=$this->renderFollowee();
        }
        
        $body.="</article>";
        if(isset($_SESSION['user_login'])){
            $body .= $this->renderBottomMenu();
        }
        $body.="</section>";
        $body.="<footer class='theme-backcolor1'>".$this->renderFooter()."</footer>";

        return $body;
        
    }   
}