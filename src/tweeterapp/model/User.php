<?php

namespace tweeterapp\model;

class User extends \Illuminate\Database\Eloquent\Model {

       protected $table      = 'user';  /* le nom de la table */
       protected $primaryKey = 'id';     /* le nom de la clé primaire */
       public    $timestamps = false;    /* si vrai la table doit contenir
                                            les deux colonnes updated_at,
                                            created_at */
       public function tweets() {
              return $this->hasMany('\tweeterapp\model\Tweet', 'author');
       }

       public function liked() {
              return $this->belongsToMany('\tweeterapp\model\Tweet', '\tweeterapp\model\Like', 'user_id', 'tweet_id');
       }

       public function followedBy() {
              return $this->belongsToMany('\tweeterapp\model\User', '\tweeterapp\model\Follow', 'followee', 'follower');
       }

       public function follows() {
              return $this->belongsToMany('\tweeterapp\model\User', '\tweeterapp\model\Follow', 'follower', 'followee');
       }

       public function alreadyFollow($idfollower,$idfollowee){
              $check=\tweeterapp\model\Follow::where("follower","=",$idfollower)->where("followee",'=',$idfollowee)->count();
              if($check > 0){
                return true;
              }else{
                return false;
              }
            } 
}

