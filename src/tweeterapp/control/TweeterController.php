<?php

namespace tweeterapp\control;

use \tweeterapp\model\Tweet as Tweet;
use \tweeterapp\model\User as User;
use \tweeterapp\model\Follow as Follow;

/* Classe TweeterController :
 *  
 * Réalise les algorithmes des fonctionnalités suivantes: 
 *
 *  - afficher la liste des Tweets 
 *  - afficher un Tweet
 *  - afficher les tweet d'un utilisateur 
 *  - afficher la le formulaire pour poster un Tweet
 *  - afficher la liste des utilisateurs suivis 
 *  - évaluer un Tweet
 *  - suivre un utilisateur
 *   
 */

class TweeterController extends \mf\control\AbstractController {

    
    public function __construct(){
        parent::__construct();
    }

    
    public function viewHome(){

        $requete = Tweet::orderBy('id', 'DESC')->limit(10);
        $lignes = $requete->get();  
        $view=new \tweeterapp\view\TweeterView($lignes);
        $view->render("home");
    }
    
    public function viewTweet(){

        if(isset($this->request->get['id'])){

            $id=$this->request->get['id'];
            $tweet=Tweet::where('id', '=', $id)->first();
            $view=new \tweeterapp\view\TweeterView($tweet);
            $view->render("viewtweet");
        }

         /*  Erreurs possibles : (*** à implanter ultérieurement ***)
         *    - pas de paramètre dans la requête
         *    - le paramètre passé ne correspond pas a un identifiant existant
         *    - le paramètre passé n'est pas un entier 
         * 
         */

    }

    
    public function viewUserTweets(){

        $id=$this->request->get['id'];
        $user=User::where('id', '=', $id)->first();
        $view=new \tweeterapp\view\TweeterView($user);
        $view->render("usertweets");
        
         /*
         *  Erreurs possibles : (*** à implanter ultérieurement ***)
         *    - pas de paramètre dans la requête
         *    - le paramètre passé ne correspond pas a un identifiant existant
         *    - le paramètre passé n'est pas un entier 
         * 
         */
        
    }

    public function viewPostTweet(){
        $view=new \tweeterapp\view\TweeterView(null);
        $view->render("posttweet");
    }

    public function viewSendTweet(){
        $filterTweet=filter_var($_POST['text']);
        if($filterTweet){
            $user = User::where("username",'=',$_SESSION['user_login'])->first();
            $tweet= new Tweet();
            $tweet->text=$_POST['text'];
            $tweet->author=$user->id;
            $tweet->save();
            $router = new \mf\router\Router();
            $router->executeRoute('default');
        }
    }

    public function following(){ 
    
        $user = User::where('username','=',$_SESSION['user_login'])->first();
        $view = new \tweeterapp\view\TweeterView($user);
        $view->render("following");
    }

    public function follow(){
        $user = User::where('username',$_SESSION['user_login'])->first();
        
        if($user->alreadyFollow($user->id, $this->request->get['id']) == false){
            if($this->request->get['id'] != $user->id){

            $follow = new Follow();
            $follow->follower = $user->id;
            $follow->followee = $this->request->get['id'];
            $follow->save();
            $nbfollowers = Follow::where('followee','=',$user->id)->count();
            $user->followers = $nbfollowers; 
            $user->save();
            }
        }

        if(isset($_GET['idtweet'])){
            $tweet = Tweet::where("id",'=',$_GET['idtweet'])->first();
            $view = new \tweeterapp\view\TweeterView($tweet);
            $view->render("viewtweet");
          }
        else{
          $view = new \tweeterapp\view\TweeterView($user);
          $view->render("followee");
        }
    }

    public function follower(){
        $user = User::where('username',$_SESSION['user_login'])->first();
        $view = new \tweeterapp\view\TweeterView($user);
        $view->render("follower");
    }

    public function followee(){
        $user = User::where('username',$_SESSION['user_login'])->first();
        $view = new \tweeterapp\view\TweeterView($user);
        $view->render("followee");
    }
}
