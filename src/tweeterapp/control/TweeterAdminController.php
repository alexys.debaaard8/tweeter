<?php

namespace tweeterapp\control;

use \tweeterapp\model\User as User;
use \tweeterapp\View\TweeterView as TweeterView;
use \mf\router\Router as Router;
use \mf\auth\Authentification as Authentification;

class TweeterAdminController extends \mf\control\AbstractController{

    public function login(){
        $view=new TweeterView(null);
        $view->render("login");
    }

    public function checkLogin(){
        $user = User::where('username',filter_var($_POST['username'], FILTER_SANITIZE_STRING))->first();
        if(!empty($user)){
            $auth = new Authentification();
            $auth->login($user->username, $user->password, $_POST['password'], $user->level);
            Router::executeRoute('following');
        }else{
            Router::executeRoute('login');
        }
    }

    public function logout(){
        $auth = new Authentification();
        $auth->logout();
        Router::executeRoute('default');
    }

    public function signup(){
        $view=new TweeterView(null);
        $view->render("signup");
    }

    public function checkSignup(){
        if($_POST['password']==$_POST['password_verify']){
            if(User::where('username','=',$_POST['username'])->first()){
                Router::executeRoute('signup');
            }else{
                $user = new User();
                $user->fullname = filter_var($_POST['fullname'], FILTER_SANITIZE_STRING );
                $user->username = filter_var($_POST['username'], FILTER_SANITIZE_STRING );
                $user->password = password_hash($_POST['password'], \PASSWORD_DEFAULT);
                $user->level = 100;    
                $user->followers = 0;  
                $user->save(); 
                Router::executeRoute('default');
            }
        }else{
            Router::executeRoute('signup');
        }
    }
}